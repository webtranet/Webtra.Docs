<?php declare(strict_types=1);
/**
 * Webtra.Docs - https://webtranet.online/wappstower/Webtra.Docs
 *
 * @link      https://gitlab.com/webtranet/Webtra.Docs.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */