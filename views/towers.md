<?php declare(strict_types=1);
/**
 * Webtra.Docs - https://webtranet.online/wappstower/Webtra.Docs
 *
 * @link      https://gitlab.com/webtranet/Webtra.Docs.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */
?>

<div class="jumbotron">
	<p class="lead">Welcome to the documentation section of Webtranet.</p>
	<hr class="my-4">
	<p><?php echo t("WEBTRA.DOCS.ARCHITECTUREDESCRIPTION"); ?></p>
</div>

Architecture
============

There are three types of tower involved in the architecture.
1. UserTower (aka Client or Browser)
2. ServiceTower
3. WappsTower


UserTower
---------
The UserTower, which is usually the Browser, is what is going to load the Web Desktop environment and runs on the end-user's machine. This is the part communicating with the ServiceTower backend and uses the WappsTower repositories.


ServiceTower
------------
The ServiceTower is the the fundament of the of tripletower architecture. It delivers the Web Desktop via the Bootloader interface. It also provides several APIs like User setting syncronization, etc.


WappsTower
----------
The WappTower is like the linux repository management. All wapps are installed on these and used by the ServiceTower. You can have more than one WappsTower and even host your own. The WappsTower doesn't need a direct connection to the ServiceTower. Communcation between these takes place via the UserTower and the IsolationGate library.
