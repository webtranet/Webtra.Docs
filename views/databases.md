<?php declare(strict_types=1);
/**
 * Webtra.Docs - https://webtranet.online/wappstower/Webtra.Docs
 *
 * @link      https://gitlab.com/webtranet/Webtra.Docs.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */
?>

<div class="jumbotron">
	<p class="lead">Welcome to the documentation section of Webtranet.</p>
	<hr class="my-4">
	<p><?php echo t("WEBTRA.DOCS.DATABASEDESCRIPTION"); ?></p>
</div>

Databases
============
Database
---------
Each database must have at least a table called versions with the columns:
* version_id with type unsigned BIGINT as primary key with auto increment
* version with type VARCHAR containing the version in the format like 1.0.1
* revisioncounter with type SMALLINT containing the revision counter
* description with type VARCHAR containing the description of the version
* developer with type VARCHAR containing the developer of the version
* revisiondate with type TIMESTAMP containing the timestamp of the version

All other tables must have at least a column holding the primary key.
* naming tablenameinsingular_id
* type unsigned BIGINT
* default value auto increment

If the database does not have the type BIGINT a corresponding type has to be chosen.

Database Connector
---------
Reading and writing to the databases have to be done with database connectors which are extending the DataBaseConnector class.