<?php declare(strict_types=1);
/**
 * Webtra.Docs - https://webtranet.online/wappstower/Webtra.Docs
 *
 * @link      https://gitlab.com/webtranet/Webtra.Docs.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */
?>

<div class="jumbotron">
	<p class="lead">Welcome to the documentation section of Webtranet.</p>
	<hr class="my-4">
	<p><?php echo t("WEBTRA.DOCS.VIEWSDESCRIPTION"); ?></p>
</div>

Views
============

View components
---------
A view consists of three files in the views folder which are automaticly merged.
* __HTML__
	This file contains the static HTML and the PHP snippets to generate the dynamic HTML code for the view.
* __CSS__
	This file contains the CSS style definitions for the view.
* __JS__
	This file contains the JS functions and logic which are needed for the view.

Metajson configuration node for views
----------
Each view has to be configured in the views part of the meta.json for the wapp application.
The parameters are:
	* __title__: Here you have to define the title for the view which is shown for example in the navigation menue.
	* __accessType__: Here you have to specify the accessability of the view like public or restricted.
	* __preRenderHook__: Here you can specify the PHP script which is executed before the page is rendered.
	* __header__: Here you can specify a custom header for the view or disable it with false. If nothing is set the default header is shown.
	* __navigation__: Here you can specify a custom navigation for the view or disable it with false. If nothing is set the default navigation is shown.
	* __content__: Here you have to specify a custom content for the view.
	* __sidebar__: Here you can specify a custom sidebar for the view or disable it with false. If nothing is set the default sidebar is shown.
	* __footer__: Here you can specify a custom footer for the view or disable it with false. If nothing is set the default footer is shown.

The values from the options: header, navigation, content, sidebar and footer contain also just contain a view name which after all constists of three components (HTML, CSS, JS) from the views folder.

Here is a example of a view configuration from the meta.json:

	{
        "default_view": "exampleview",

        "exampleview":
        {
            "title "#YOURCOMPANY.YOURWAPP.WELCOMETITLE",
            "accessType "public",
            "preRenderHook "preRenderHook.php",
            "header false,
            "navigation true,
            "content "exampleview",
            "sidebar false,
            "footer false
        }

        ...
    }
	
