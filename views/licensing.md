<?php declare(strict_types=1);
/**
 * Webtra.Docs - https://webtranet.online/wappstower/Webtra.Docs
 *
 * @link      https://gitlab.com/webtranet/Webtra.Docs.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */
?>

<div class="jumbotron">
	<p class="lead">Welcome to the documentation section of Webtranet.</p>
	<hr class="my-4">
	<p><?php echo t("WEBTRA.DOCS.LICENSINGDESCRIPTION"); ?></p>
</div>

Licensing
=========
As every software also the tripletower and tripletower-sdk have a license. It's called ONFSL and is pretty permissive. But it's not a free software license because it restricts you from making _direct_ money with it.

License text
------------
<pre>
	<?php
		include $_SERVER["TT"]->tower->getPublicFolder()."LICENSE";
	?>
</pre>


Contribution
------------
<?php echo t("WEBTRA.DOCS.HOWTOCONTRIBUTE"); ?>


Support
-------
<?php echo t("WEBTRA.DOCS.HOWTOGETSUPPORT"); ?>