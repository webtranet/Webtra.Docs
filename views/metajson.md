<?php declare(strict_types=1);
/**
 * Webtra.Docs - https://webtranet.online/wappstower/Webtra.Docs
 *
 * @link      https://gitlab.com/webtranet/Webtra.Docs.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */
?>

<div class="jumbotron">
	<p class="lead">Welcome to the documentation section of Webtranet.</p>
	<hr class="my-4">
	<p><?php echo t("WEBTRA.DOCS.METAJSONDESCRIPTION"); ?></p>
</div>

Meta json
=========
This file is the glue which holds all components together.

These are the necessary and available entries:

name (`string`)
> The unique name of the wapp e.g. `MyCompany.NiceWapp`

version (`string`)
> The current version of the wapp e.g. `1.5.2`

versionName (`string`)
> A nice version name for the user to remember important versions e.g. `Awesomion`

versionDate (`string`)
> The date the version got updated. There is no standardized format yet. E.g. `15.05.2022`

description (`string`)
> A description what your wapp does. It goes into the wapp catalog for the users to read, it will be visible in the info button at the bottom of your navigation and it goes into the top of your page as meta information for crawlers. e.g. This wapp does make a lot of cool things. You should propably use it.

changeLog (`bool`/`string`)
> Your changelog file should contain information about what big things came in what version.

> `false` - Don't not show any changelog infos.

> `true` - Use the changelog file named `CHANGELOG` in the wapp's root folder

> `string` - Use the given name as file name to look into the wapp's root folder. Supported are `md`, `html` or `txt` file endings. All other file endings will be treated as `txt`. e.g. `CHANGELOG.md`

license (`string`)
> The license name. Don't forget to include the LICENSE file in the root of your wapp. e.g. `ONFSLv1`

developer (`object`)
> Holding developer infos like name (`string`), url (`string`) and email (`string`). This are the contact information available to the user in the wapp catalog e.g.

		{
			"name "The example codes",
			"url "https://www.example.com",
			"email "info@example.com"
		}

mediaUrls (`array` of `strings`)
> Urls to images and videos which are being shown in the wapps catalog. e.g.

		[
			"https://example.com/example.png",
			"/wappstower/MyCompany.ExampleWapp/public/example2.mp4"
		]

categories (`array` of `strings`)
> Categories for being used as filters in the wapp catalog. e.g.

		[
			"Office",
			"Games"
		]

requirements (`object` of `strings`)
> Information for the user shown in the wapp catalog what is necessary to use the wapp e.g.

	{
		Edge: 12,
		FireFox: 4,
		Chrome 1,
	}


icons (`object` of `strings`)
> A list of icons of the wapp. Used in the wapp catalog, navigation, as favicon and in the window title bar. e.g. The icon has to be a square and the text index has to indiciate the resolution. e.g.

		{
			"32": "/MyCompany.MyWapp/public/logo_32.png",
			"128": "/MyCompany.MyWapp/public/logo_32.png",
			"256": "/MyCompany.MyWapp/public/logo_32.png"
		}

visible (`bool`)
> If the wapp should be visible in the wapp catalog. e.g. `true`/`false`

permissions (`array` of `strings`)
> These are the permissions the wapp needs to access your personal data. e.g.

		[
			"Events.onGetStyle.trigger",
			"Services.Fellows.Get"
		]

manifestVersion (`int`)
> This is the version which defines the structure of this meta.json file. e.g. `1`

startType (`string`)
> In which window mode should the wapp start. e.g. `fullscreen`, `windowed` or `minimized`

startHeight (`int`)
> If the wapp starts in windowd mode which hight resolution should it have. e.g. `600`

startWidth (`int`)
> If the wapp starts in windowd mode which width resolution should it have. e.g. `800`

resizable (`bool`)
> If the window should be resizable. e.g. `true`/`false`

fullscreenable (`bool`)
> If the window can go in fullscreen mode e.g. `true`/`false`

appcachePath (`string`)
> The path to the appcache file. See https://developer.mozilla.org/docs/Web/API/Window/applicationCache for more information. e.g. `/wappstower/YourCompany.YourWapp/public/cache.appcache`

dependencies (`object` of `arrays`)
> Holds all css and js files necessary for your wapp to work. These will be loaded in every view. e.g.

		{
			"css":
			[
				// Your CSS dependencies go in here and these will be loaded 
				// in every view. Usage e.g.:
				
				"/servicetower/libs/wdl/1.1/css/wdl.css"
			],
			"js":
			[
				// Your JS dependencies go in here and these will be loaded 
				// in every view. It supports js scripts and esm 
				// modules. Usage e.g.:
				
				"/servicetower/libs/wdl/1.1/js/Wdl.esm.js"
			]
		},
views (`object`)
> Defines what views are available and which should be the default. The view part is so complex that it got it's own documentation. e.g.

		{
			"default_view": "exampleview",
			
			"exampleview":
			{
				"title "#YOURCOMPANY.YOURWAPP.WELCOMETITLE",
				"accessType "public",
				"preRenderHook "preRenderHook.php",
				"header false,
				"navigation true,
				"content "exampleview",
				"sidebar false,
				"footer false
			}
			
			...
		}

api (`object`)
> Defines the available rest APIs and what the expected parameters are. The API part is so complex that it got it's own documentation. e.g.

		"yourApiName":
		{
			"accessType": "public",
			"description": "Your description goes here...",
			"conditions":
			[
				{ "variable": "paramName1", "condition": "type", "value": "string" },
				{ "variable": "paramName1", "condition": "callback", "value": "checkMyVariable" },
				{ "variable": "validateSomeId", "condition": "type", "value": "int" }
			]
		}

