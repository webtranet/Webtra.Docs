<?php declare(strict_types=1);
/**
 * Webtra.Docs - https://webtranet.online/wappstower/Webtra.Docs
 *
 * @link      https://gitlab.com/webtranet/Webtra.Docs.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */
?>

<div class="jumbotron">
	<p class="lead">Welcome to the documentation section of Webtranet.</p>
	<hr class="my-4">
	<p><?php echo t("WEBTRA.DOCS.CONFIGURATIONDESCRIPTION"); ?></p>
</div>

Configuration
============

Types
---------
There are three types of configuration files.
1. __Global__ (global.json)
	This config stores the information which is needed for the CLI scripts *and* the wapp.
	
2. __Wapp__ (wappconfig.json)
	This config stores the information which is only needed for the wapp.
	
3. __Instance__
	This are config files which are automatically loaded by the CLI-Tower, depending on the given instance name or config files which are loaded on demand by user code.

Content
---------
There are three main configuration nodes for each config. The base configuration is done in the development node. In the other nodes only the information which is special for the according environment has to be specified because the nodes get merged from development to the actual environment level. It's working like inheritance in OOP. Here is the structure of a configuration file:

	{
		"development":
		{

		},

		"staging":
		{

		},

		"production":
		{

		}
	}

