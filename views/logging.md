<?php declare(strict_types=1);
/**
 * Webtra.Docs - https://webtranet.online/wappstower/Webtra.Docs
 *
 * @link      https://gitlab.com/webtranet/Webtra.Docs.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */
?>

<div class="jumbotron">
	<p class="lead">Welcome to the documentation section of Webtranet.</p>
	<hr class="my-4">
	<p><?php echo t("WEBTRA.DOCS.LOGGINGDESCRIPTION"); ?></p>
</div>

Logging
============

TripleTowerErrors are automatically logged depending on the severity set in the error and the verbosity set in the config file. For file based logging the log files go into the temp folder. If the severity is high enough additionally a mail is being sent to the tower administrator. Custom log entries can be written by using the logger object $_SERVER["TT"]->logger and calling its functions:
* notice
* info
* debug
* err
* alert
* crit
* emerg

This list represents also the severity levels of the log entry. 