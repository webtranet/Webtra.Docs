<?php declare(strict_types=1);
/**
 * Webtra.Docs - https://webtranet.online/wappstower/Webtra.Docs
 *
 * @link      https://gitlab.com/webtranet/Webtra.Docs.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */
?>

<div class="jumbotron">
	<p class="lead">Welcome to the documentation section of Webtranet.</p>
	<hr class="my-4">
	<p><?php echo t("WEBTRA.DOCS.APISDESCRIPTION"); ?></p>
</div>

APIs
============

API PHP files
---------
The php files that contain the api functions have to be located in the api folder of the wapp. The name of the file itself doesn't matter. They just get all included, so beware of same function names. The api function gets an array containing the parameters which are specified in the api section of the meta.json file. The api function is simply a plain php function which has to return a TripleTowerError. In case of success the function shas to set the mime type in the response header, echo the result body and return an SUCCESS TripleTowerError object. In the example below, a SUCCESS TripleTowerError object gets created and serialized. The header is set by the serializer by setting the second function parameter to true.
The serialized object is echoed and the SUCCESS TripleTowerError object gets retured.

```
	function myApiFunction( $argv )
	{
		$result = new ApiError(ApiError::SUCCESS);
		echo Serializer::jsonSerialize($result, true);
		return result;
	}
```

Meta.json configuration node for api functions
---------
For each accesible api function a configuration node with the name of api function has to be created. This node must contain the following information:
* accesType: Define if it's access is restricted or not.
* description: A short description of what the function does.
* conditions: If you need to pass parameters to the function you have to specify them at least with one condition namely the condition type. Each condition needs the following parameters:
	*  variable: name of the parameter
	*  condition: type of the condition like parameter type
	*  value: the value of the condition like the name of the parameter type

```
	{
		...
		
		"api":
		{
			"myApiFuction":
			{
				"accessType": "public",
				"description": "Your description goes here...",
				"conditions":
				[
					{ "variable": "paramName1", "condition": "type", "value": "string" },
					{ "variable": "paramName1", "condition": "callback", "value": "checkMyVariable" },
					{ "variable": "validateSomeId", "condition": "type", "value": "int" }
				]
			}
			
			...
		}
	}
```