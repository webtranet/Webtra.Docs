<?php declare(strict_types=1);
/**
 * Webtra.Docs - https://webtranet.online/wappstower/Webtra.Docs
 *
 * @link      https://gitlab.com/webtranet/Webtra.Docs.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */
?>

<div class="jumbotron">
	<p class="lead">Welcome to the documentation section of Webtranet.</p>
	<hr class="my-4">
	<p><?php echo t("WEBTRA.DOCS.TEMPDESCRIPTION"); ?></p>
</div>

Temp
============

The temp folder contains the log files created by the logging facility. If optimization is enabled also the dumplings will be saved in there. Furthermore all your temporary data should go in there.