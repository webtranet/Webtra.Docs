<?php declare(strict_types=1);
/**
 * Webtra.Docs - https://webtranet.online/wappstower/Webtra.Docs
 *
 * @link      https://gitlab.com/webtranet/Webtra.Docs.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */
?>

<div class="jumbotron">
	<p class="lead">Welcome to the documentation section of Webtranet.</p>
	<hr class="my-4">
	<p><?php echo t("WEBTRA.DOCS.IDESDESCRIPTION"); ?></p>
</div>

IDEs
============

Currently there are two supported code editors.
1. Apache Netbeans
2. VSCodium

Apache Netbeans
---------

*What is it?*
NetBeans has originally been written as and IDE for Java. But it's not only limited to Java development, it has extensions for other languages like PHP, C, C++, HTML5 and JavaScript.

It started development 1996 on the Faculty of Mathematics and Physics at Charles University in Prague, being commercialized on 1997, being bought by Sun Microsystems in 1999 where it got open sourced. Sun got bought by Oracle on 2010 and Netbeans got handed over to the apache foundation in 2016.

*Why NetBeans*
It is one of the best open source IDEs for PHP. It outperformes direct competitors like Codelobster, Aptana Studio, Notpad++ with plugins, Visual Studio with plugin and Zend Studio.
The only better editor is PHP Storm which is neither free open source software, nor freeware.

It runs on many major operating systems:
- Windows
- Linux
- macOS
- Solaris

It supports various programming languages:
- PHP
- C/C++
- HTML5
- JavaScript

Netbeans offers the following features:
- Syntax highlighting, occurrence and error highlighting
- Code completion
- Git version control
- SQL Statement completion
- Semantic analysis with highlighting of parameters and unused local variables
- PHP code debugging with xdebug
- PHP testing with PHPUnit and Selenium
- Code coverage
- PHP 5.3 namespace and closure support
- Code Folding for Control Structures

It extends easily with plugins.
Here are the ones we use:
- Darcula
- Brainbow Braces
- Quick Opener

Vendor: https://netbeans.apache.org

Wiki: https://en.wikipedia.org/wiki/NetBeans

VSCodium
----------
VSCodium is a community-driven, freely-licensed binary distribution of Microsoft’s editor VS Code. It is currently in evaluation phase since VSCodium is getting better and better.

Vendor: https://vscodium.com

Wiki: https://en.wikipedia.org/wiki/Visual_Studio_Code